# MotionPlanHomework

## 介绍
深蓝学院运动规划作业集

## 目录结构
### 第一章作业

作业源码：无  
作业说明文档：[homework-L1](https://gitee.com/lxyclara/motion-plan-homework/tree/master/L1)

### 第二章作业：Search-Based Method

作业源码：[source-L2](https://gitee.com/lxyclara/motion-plan-homework/tree/lxy/L2/src/grid_path_searcher)  
作业说明文档：[homework-L2](https://gitee.com/lxyclara/motion-plan-homework/blob/master/L2/homework.md)

### 第三章作业

作业源码：[source-L3](https://gitee.com/lxyclara/motion-plan-homework/tree/lxy/L3/src/path_finder)  
作业说明文档：[homework-L3](https://gitee.com/lxyclara/motion-plan-homework/blob/lxy/L3/homework.md)  

### 第四章作业

作业源码：[source-L4](https://gitee.com/lxyclara/motion-plan-homework/tree/lxy/L4/src/grid_path_searcher)  
作业说明文档：[homework-L4](https://gitee.com/lxyclara/motion-plan-homework/blob/lxy/L4/homework.md)

### 第五章作业

作业源码：[source-L5](https://gitee.com/lxyclara/motion-plan-homework/tree/lxy/L5/src/src)  
作业说明文档：[homework-L5](https://gitee.com/lxyclara/motion-plan-homework/blob/lxy/L5/homework.md)

### 第六章作业

作业源码：[source-L6](https://gitee.com/lxyclara/motion-plan-homework/tree/lxy/L6/src/mpc_car)    
作业说明文档：[homework-L6](https://gitee.com/lxyclara/motion-plan-homework/blob/lxy/L6/homework.md)  

### 第七章作业

作业源码：source-L7  
作业说明文档：homework-L7

### 第八章作业

作业源码：source-L8  
作业说明文档：homework-L8




